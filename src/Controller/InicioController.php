<?php

    namespace App\Controller;

    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\Routing\Annotation\Route;

    /**
     * Class InicioController
     *
     * @package App\Controller
     */
    class InicioController extends AbstractController{

        /**
         * @Route(
         *     path="/inicio",
         *     name="inicio",
         *     methods={"GET"}
         * )
         */
        public function index(){
            return $this->render('inicio/index.html.twig', []);
        }

        /**
         * @Route(
         *     path="/contacto",
         *     name="contacto",
         *     methods={"GET"}
         * )
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function contact(){
            return $this->render('inicio/contact.html.twig');
        }

        /**
         * @Route(
         *     path="/login",
         *     name="login",
         *     methods={"GET"}
         * )
         */
        public function login(){
            return $this->render('inicio/login.html.twig');
        }

        /**
         * @Route(
         *     path="/tracking",
         *     name="tracking",
         *     methods={"GET"}
         * )
         * @return \Symfony\Component\HttpFoundation\Response
         * https://colorlib.com/wp/template/karma/
         */
        public function tracking(){
            return $this->render('inicio/tracking.html.twig');
        }

        /**
         * @Route(
         *     path="element",
         *     name="element",
         *     methods={"GET"}
         * )
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function elements(){
            return $this->render('inicio/element.html.twig');
        }

        /**
         * @Route(
         *     path="/mostrar/{nombre}/{apellido}",
         *     name="mostrar_nombre",
         *     defaults={ "nombre" = "test", "apellido" = "Prueba" },
         *     methods={"GET"}
         * )
         *
         * @param string|null $nombre
         * @return \Symfony\Component\HttpFoundation\Response
         */
        public function getMostrarNombre(?string $nombre, ?string $apellido){

            return $this->render('inicio/nombre.html.twig', [
                'nombre' => $nombre,
                'apellido' => $apellido,

            ]);
        }

    }
